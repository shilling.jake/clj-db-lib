(disable-warning
  {:linter :constant-test
   ;:for-macro 'next.jdbc/with-transaction
   :for-macro 'clojure.core/let
   :if-inside-macroexpansion-of #{'next.jdbc/with-transaction}
   :reason "Doesn't function properly for some reason..."})

(disable-warning
 {:linter :suspicious-expression
  :for-macro 'clojure.core/or
  :if-inside-macroexpansion-of #{'clj-data-lib.data/or+}
  :reason "Doesn't function properly for some reason..."})