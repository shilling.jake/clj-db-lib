(ns clj-db-lib.utils.exceptions)

(defn validation-error [msg e]
  (ex-info msg {:validate-error e}))